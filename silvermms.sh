#!/bin/bash
which recoverjpeg || sudo apt-get install -y recoverjpeg
for modem in {0..100}; do mmcli -m $modem --messaging-list-sms && break; done
mmcli -m $modem --messaging-list-sms | cut -f 6 -d '/' | grep received | awk '{print $1}' | while read smsnum; 
do 
        mmcli -m $modem -s $smsnum --create-file-with-data=$smsnum.data.sms ; 
        mmcli -m $modem -s $smsnum ;
        i=0;
        cat $smsnum.data.sms | tr -c '[:print:]' '\n' | grep http | while read file; 
        do 
		#If you have problems with wget, make sure the request is on the correct
		#interface and protocol. You might need to make sure only your carrier dns
		#server is present in /etc/resolv.conf, and (at least for tmo) only ipv6
                wget "$file" -O $smsnum.data.sms.file.$i; 
                mkdir $smsnum.data.sms.file.$i.out;
                recoverjpeg -b1 $smsnum.data.sms.file.$i -o $smsnum.data.sms.file.$i.out || rm -rf $smsnum.data.sms.file.$i.out; 
                i=$(($i + 1));
        done;
        #comment out if you don't want to delete messages from the modem 
        mmcli -m $modem --messaging-delete-sms $smsnum; 
        echo; 
done